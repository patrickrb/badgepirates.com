import { Component, OnInit } from '@angular/core';
import { BADGES } from './badges';
import * as bootstrap from "bootstrap";

@Component({
  selector: 'app-badges',
  templateUrl: './badges.component.html',
  styleUrls: ['./badges.component.less'],
})
export class BadgesComponent implements OnInit {
  closeResult: string;

  badges = BADGES;


  ngOnInit() {}

  open(id) {
    $(`#badge-${id}`).modal('show');
  }
}
